from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QMessageBox
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QFileDialog
from ui_py import ui_main_window
from windows import dialog_window
import csv

class MainWindow(QMainWindow, ui_main_window.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_add.clicked.connect(self.btn_add_click)
        self.pushButton_edit.clicked.connect(self.btn_edit_click)
        self.pushButton_delite.clicked.connect(self.btn_del_click)
        self.pushButton_record.clicked.connect(self.btn_record_click)
        # self.radio_ping.clicked.connect()
        # self.radio_trancert.clicked.connect()
        # self.radio_mask.clicked.connect()

        QTimer.singleShot(1, self.onStart)

    def onStart(self):
        with open("data\ip_addresses.csv", 'r', encoding='utf-8', newline='') as f:
            rd = csv.DictReader(f, delimiter=';')
            self.tableWidget.clear()
            headers = rd.fieldnames
            self.tableWidget.setColumnCount(len(headers))
            self.tableWidget.setHorizontalHeaderLabels(headers)
            for i, row in enumerate(rd):
                self.tableWidget.setRowCount(i + 1)
                for j, (key, value) in enumerate(row.items()):
                    self.tableWidget.setItem(i, j, QTableWidgetItem(value))

    def closeEvent(self, event) -> None:
        with open("data\ip_addresses.csv", 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([
                self.tableWidget.horizontalHeaderItem(0).text(),
                self.tableWidget.horizontalHeaderItem(1).text(),
                self.tableWidget.horizontalHeaderItem(2).text(),
                self.tableWidget.horizontalHeaderItem(3).text(),
                self.tableWidget.horizontalHeaderItem(4).text(),
                self.tableWidget.horizontalHeaderItem(5).text()
            ])
            for i in range(self.tableWidget.rowCount()):
                line = []
                line.append(self.tableWidget.item(i, 0).text())
                line.append(self.tableWidget.item(i, 1).text())
                line.append(self.tableWidget.item(i, 2).text())
                line.append(self.tableWidget.item(i, 3).text())
                line.append(self.tableWidget.item(i, 4).text())
                line.append(self.tableWidget.item(i, 5).text())
                writer.writerow(line)

    def btn_record_click(self):
        fd = QFileDialog.getSaveFileName(self,
                                         f'{self.windowTitle()} [путь для сохранения]',
                                         '',
                                         'CSV (*.csv);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'w', encoding='utf-8', newline='') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerow([
                    self.tableWidget.horizontalHeaderItem(0).text(),
                    self.tableWidget.horizontalHeaderItem(1).text(),
                    self.tableWidget.horizontalHeaderItem(2).text(),
                    self.tableWidget.horizontalHeaderItem(3).text(),
                    self.tableWidget.horizontalHeaderItem(4).text(),
                    self.tableWidget.horizontalHeaderItem(5).text()
                ])
                for i in range(self.tableWidget.rowCount()):
                    line = []
                    line.append(self.tableWidget.item(i, 0).text())
                    line.append(self.tableWidget.item(i, 1).text())
                    line.append(self.tableWidget.item(i, 2).text())
                    line.append(self.tableWidget.item(i, 3).text())
                    line.append(self.tableWidget.item(i, 4).text())
                    line.append(self.tableWidget.item(i, 5).text())
                    writer.writerow(line)

    def btn_add_click(self):
        dlg = dialog_window.DialogAdd(self, mode='add', app_name=self.windowTitle())
        dlg.show()
        res = dlg.exec_()
        if res:
            data = dlg.get_data()
            row = self.tableWidget.rowCount()
            self.tableWidget.setRowCount(row + 1)
            self.tableWidget.setItem(row, 0, QTableWidgetItem(data['oktet1']))
            self.tableWidget.setItem(row, 1, QTableWidgetItem(data['oktet2']))
            self.tableWidget.setItem(row, 2, QTableWidgetItem(data['oktet3']))
            self.tableWidget.setItem(row, 3, QTableWidgetItem(data['oktet4']))
            self.tableWidget.setItem(row, 4, QTableWidgetItem(data['mask']))
            self.tableWidget.setItem(row, 5, QTableWidgetItem(data['description']))

            if data != 0 and self.checkBox.isChecked():
                self.textEdit.setText(
                    f"{self.tableWidget.item(row, 0).text()}.{self.tableWidget.item(row, 1).text()}.{self.tableWidget.item(row, 2).text()}.{self.tableWidget.item(row, 3).text()}\{self.tableWidget.item(row, 4).text()} {self.tableWidget.item(row, 5).text()}")
            else:
                self.textEdit.append(
                    f"{self.tableWidget.item(row, 0).text()}.{self.tableWidget.item(row, 1).text()}.{self.tableWidget.item(row, 2).text()}.{self.tableWidget.item(row, 3).text()}\{self.tableWidget.item(row, 4).text()} {self.tableWidget.item(row, 5).text()}")

    def btn_edit_click(self):
        row = self.tableWidget.currentRow()
        if row != -1:
            data = {
                'oktet1': self.tableWidget.item(row, 0).text(),
                'oktet2': self.tableWidget.item(row, 1).text(),
                'oktet3': self.tableWidget.item(row, 2).text(),
                'oktet4': self.tableWidget.item(row, 3).text(),
                'mask': self.tableWidget.item(row, 4).text(),
                'description': self.tableWidget.item(row, 5).text(),
            }
            dlg = dialog_window.DialogAdd(self,
                                            mode='edit',
                                            app_name=self.windowTitle(),
                                            data=data)
            dlg.show()
            res = dlg.exec_()
            if res:
                data = dlg.get_data()
                self.tableWidget.item(row, 0).setText(data['oktet1'])
                self.tableWidget.item(row, 1).setText(data['oktet2'])
                self.tableWidget.item(row, 2).setText(data['oktet3'])
                self.tableWidget.item(row, 3).setText(data['oktet4'])
                self.tableWidget.item(row, 4).setText(data['mask'])
                self.tableWidget.item(row, 5).setText(data['description'])
        else:
            QMessageBox.warning(self,
                                self.windowTitle(),
                                'Строка для редактирования не выбрана!')

    def btn_del_click(self):
        msg = QMessageBox.question(self,
                                   self.windowTitle(),
                                   'Вы действительно хотите удалить запись?',
                                   QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            row = self.tableWidget.currentRow()
            self.tableWidget.removeRow(row)