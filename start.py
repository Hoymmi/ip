from PyQt5.QtWidgets import QApplication
from windows import main_window

if __name__ == '__main__':
    app = QApplication([])
    wnd = main_window.MainWindow()
    wnd.show()
    app.exec_()
